// document.querySelector('.scroll__top').addEventListener()
window.addEventListener('scroll', () => {
    const scrollTop = document.querySelector('.scroll__top');
    if (window.scrollY > window.screenY) {
        scrollTop.classList.remove('scroll__top--hide')
    } else {
        scrollTop.classList.add('scroll__top--hide')
    }
})