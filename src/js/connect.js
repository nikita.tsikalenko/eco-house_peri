const connectBtn = document.querySelector('.connect__btn');
connectBtn.addEventListener('click', function (){
    this.classList.toggle('connect__btn--open');
    this.closest('.connect').querySelector('.connect__list').classList.toggle('connect__list--hide');
})