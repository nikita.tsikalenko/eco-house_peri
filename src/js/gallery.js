const findActive = (slides) => {
    for (let i = 0; i < slides.length; i++) {
        if (slides[i].classList.contains('gallery__slide--active')) {
            return i;
        }
    }
}

function showSlidesButton() {
    const gallery = this.closest('.gallery').querySelectorAll('.gallery__slide');
    const dots = this.closest('.gallery').querySelectorAll('.gallery__dot');
    let previousIndex = findActive(gallery);
    let nextIndex = previousIndex + +this.dataset.sign;
    if (nextIndex > gallery.length - 1) {
        nextIndex = 0
    } else if (nextIndex < 0) {
        nextIndex = gallery.length - 1
    }
    gallery[previousIndex].classList.remove('gallery__slide--active');
    gallery[nextIndex].classList.add('gallery__slide--active');
    dots[previousIndex].classList.remove('gallery__dot--active');
    dots[nextIndex].classList.add('gallery__dot--active');
}

function showSlidesDot() {
    const gallery = this.closest('.gallery').querySelectorAll('.gallery__slide');
    const dots = this.closest('.gallery').querySelectorAll('.gallery__dot');
    let previousIndex = findActive(gallery);
    gallery[previousIndex].classList.remove('gallery__slide--active');
    gallery[this.dataset.num].classList.add('gallery__slide--active');
    dots[previousIndex].classList.remove('gallery__dot--active');
    dots[this.dataset.num].classList.add('gallery__dot--active');
}

const setGalleryListener = (galleryID) => {
    const gallery = document.querySelector(`#${galleryID}`);
    const gallerySlides = gallery.querySelectorAll('.gallery__dot');
    gallerySlides.forEach((slide, index) => {
        slide.dataset.num = index;
        slide.addEventListener('click', showSlidesDot)
    })
    const next = gallery.querySelector('.gallery__next');
    next.dataset.sign = '1';
    next.addEventListener('click', showSlidesButton);
    const prev = gallery.querySelector('.gallery__prev');
    prev.dataset.sign = '-1';
    prev.addEventListener('click', showSlidesButton);
}

setGalleryListener('gallery1');
setGalleryListener('gallery2');
setGalleryListener('gallery3');
setGalleryListener('gallery4');